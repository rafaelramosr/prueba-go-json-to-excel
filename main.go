package main

import "C"

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/xuri/excelize/v2"
)

type Data struct {
	SapCode     string `json:"sapCode"`
	Description string `json:"description"`
	Count       int    `json:"count"`
	Unit        string `json:"unit"`
	System      string `json:"system"`
	Project     string `json:"project"`
	Production  string `json:"production"`
	Use         string `json:"use"`
	SystemId    int    `json:"systemId"`
	OrderId     int    `json:"orderId"`
	OrderCO     string `json:"orderCO"`
	Line        string `json:"line"`
	Week        string `json:"week"`
	Year        string `json:"year"`
}

//export ExcelGenerate
func ExcelGenerate(jsonPath string, sheetName string) {
	// Read json file
	file, err := os.Open(jsonPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	jsonFile, err := io.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Decoding the JSON file into a Go data structure
	var data []Data
	err = json.Unmarshal(jsonFile, &data)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Create a new excel file
	f := excelize.NewFile()

	// Add a sheet
	f.NewSheet(sheetName)

	// Header
	f.SetCellValue(sheetName, "A1", "Código sap")
	f.SetCellValue(sheetName, "B1", "Descripción")
	f.SetCellValue(sheetName, "C1", "Cantidad")
	f.SetCellValue(sheetName, "D1", "Unidad")
	f.SetCellValue(sheetName, "E1", "Sistema")
	f.SetCellValue(sheetName, "F1", "Proyecto")
	f.SetCellValue(sheetName, "G1", "Producción")
	f.SetCellValue(sheetName, "H1", "Uso")
	f.SetCellValue(sheetName, "I1", "ID sistema")
	f.SetCellValue(sheetName, "J1", "ID Orden")
	f.SetCellValue(sheetName, "K1", "Orden CO")
	f.SetCellValue(sheetName, "L1", "Línea")
	f.SetCellValue(sheetName, "M1", "Semana")
	f.SetCellValue(sheetName, "N1", "Año")

	// Write the data in the cells
	for i, d := range data {
		f.SetCellValue(sheetName, fmt.Sprintf("A%d", i+2), d.SapCode)
		f.SetCellValue(sheetName, fmt.Sprintf("B%d", i+2), d.Description)
		f.SetCellValue(sheetName, fmt.Sprintf("C%d", i+2), d.Count)
		f.SetCellValue(sheetName, fmt.Sprintf("D%d", i+2), d.Unit)
		f.SetCellValue(sheetName, fmt.Sprintf("E%d", i+2), d.System)
		f.SetCellValue(sheetName, fmt.Sprintf("F%d", i+2), d.Project)
		f.SetCellValue(sheetName, fmt.Sprintf("G%d", i+2), d.Production)
		f.SetCellValue(sheetName, fmt.Sprintf("H%d", i+2), d.Use)
		f.SetCellValue(sheetName, fmt.Sprintf("I%d", i+2), d.SystemId)
		f.SetCellValue(sheetName, fmt.Sprintf("J%d", i+2), d.OrderId)
		f.SetCellValue(sheetName, fmt.Sprintf("K%d", i+2), d.OrderCO)
		f.SetCellValue(sheetName, fmt.Sprintf("L%d", i+2), d.Line)
		f.SetCellValue(sheetName, fmt.Sprintf("M%d", i+2), d.Week)
		f.SetCellValue(sheetName, fmt.Sprintf("N%d", i+2), d.Year)
	}

	// Save the excel file
	err = f.SaveAs("datos.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
}

func main() {}
